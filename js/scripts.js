jQuery(function($){

	/* load iframe panos on click */
	$('.load_iframe_wrap').click(function(){
		let video_link = $(this).find('.video_link').data('video_link');
		$(this).find('.vc_column-inner').append('<iframe class="load_iframe" src="'+ video_link +'" allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen"></iframe>');
	});

	/* disable swipe owl carousel 
	https://stackoverflow.com/questions/22909399/disable-dragging-in-specific-element-item-in-owl-carousel-jquery
	*/
	function disableOwlLimitedItem(){
		if($('.owl-nav').hasClass('disabled')){
			console.log('changed');
			$(".dt-owl-item").on("touchstart mousedown", function(e) {
				e.stopPropagation();
			});
		}
	}

	disableOwlLimitedItem();
	window.resize = disableOwlLimitedItem();


 	// var carousel = $('.owl-carousel').data('owl.carousel');
 	// carousel.destroy();
});
